# coding=utf-8
import json
from time import sleep
from sqlalchemy.exc import IntegrityError

from .config import load_config_file, get_valid_token
from .queue import UniqueQueue
from .db import init_db
from .tw import make_tweets_table, tw_to_columns, tw_token, tw_verify_token, \
    tw_stream, tw_search
from .exc import MissingConfigKeyError, NoValidEndpointError
from .utils import get_database_name


class Kijker(object):
    def __init__(self, kijker_type, delay, config_file, logger):
        self._interval = 90
        self._supported_types = frozenset({'twitter', 'twitter_search'})
        self.logger = logger
        self.delay = delay
        self.type = kijker_type.lower()
        if self.type not in self._supported_types:
            raise ValueError('Invalid type for {}: {}.'.format(__name__, self.type))
        self._uses_queue = bool(self.delay)
        token_loc, queries, database, table, queue_name = load_config_file(config_file)
        self.queries = queries
        if self._uses_queue:
            if not token_loc or not queries or not queue_name:
                raise MissingConfigKeyError
            self.queue = UniqueQueue(queue_name)
            self.logger.info(u'Searching {}, queueing IDs in {}'
                             .format(u' '.join(map(str, self.queries)),
                                     self.queue))
        else:
            if not token_loc or not queries or not (database and table):
                raise MissingConfigKeyError
            self.table = table
            self.database = database
            self.logger.info(u'Searching {}, writing results to {}/{}'
                             .format(u' '.join(map(str, self.queries)),
                                     get_database_name(self.database),
                                     self.table))
        self.api_token = get_valid_token(token_loc, tw_token, tw_verify_token)
        if len(self.queries) > 1:
            self.logger.warning(u'Multiple queries are not supported for '
                                u'this type. Only searching the first query.')
        if not self._uses_queue:
            self.db_insert = init_db(database, table, make_tweets_table)

    def loop(self):
        try:
            if self.type == 'twitter':
                self._twitter_track()
            elif self.type == 'twitter_search':
                self._twitter_search()
        except (KeyboardInterrupt, SystemExit):
            self.logger.info('Quitting.')

    def _twitter_search(self):
        max_id = None
        while True:
            try:
                tweets, new_max = tw_search(self.queries[0], self.api_token, max_id)
            except TypeError:
                self.logger.exception(u'No tweets found.')
                raise
            self.logger.info(u'Found {} tweets.'.format(len(tweets)))
            for tweet in tweets:
                tid = str(tweet['id'])
                if self._uses_queue:
                    result = self.queue.put(tid, self.delay)
                    if result:
                        self.logger.info(u'{} successfully queued'
                                         .format(tid))
                    else:
                        self.logger.warning(u'{} not queued'
                                            .format(tid))
                else:
                    try:
                        self.db_insert(**tw_to_columns(tweet))
                        self.logger.info(u'{} added to table {}.'
                                         .format(tid, self.table))
                    except IntegrityError:
                        self.logger.warning(u'{} already in table {}.'
                                            .format(tid, self.table))
                        pass
            if len(tweets) == 1:
                max_id = None
                sleep(self._interval)
            else:
                max_id = new_max

    def _twitter_track(self):
        q = self.queries[0].replace(' AND ', ' ').replace(' OR ', ', ')
        for tweet in tw_stream(q, self.api_token).iter_lines():
            if tweet:
                t = json.loads(tweet.decode())
                tid = str(t['id'])
                if self._uses_queue:
                    result = self.queue.put(tid, self.delay)
                    if result:
                        self.logger.info(u'{} successfully queued'
                                         .format(tid))
                    else:
                        self.logger.warning(u'{} not queued'.format(tid))
                else:
                    try:
                        self.db_insert(**tw_to_columns(t))
                    except IntegrityError as e:
                        self.logger.warning(u'{} already in table {}.'
                                            .format(tid, self.table))
                        pass
