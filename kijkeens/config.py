# coding=utf-8
import os
from glob import glob
from random import shuffle
from yaml import load as yaml_load

from .exc import MissingConfigFileError, NoValidTokenError


def load_config_file(f):
    if not os.path.exists(f):
        raise MissingConfigFileError('Specified configuration file does not exist.')
    with open(f, 'r') as fh:
        config = yaml_load(fh)
    token = config.get('token', None)
    if 'queries' in config:
        if isinstance(config['queries'], dict):
            queries = config['queries'].values()
        elif isinstance(config['queries'], list):
            queries = config['queries']
        else:
            queries = [config['queries']]
    elif 'query' in config:
        queries = [config['query']]
    else:
        queries = None
    queue = config.get('queue', None)
    database = config.get('database', None)
    table = config.get('table', None)
    return (token, queries, database, table, queue)


def get_valid_token(loc, token_func, verify_func=lambda t: True):
    if os.path.isdir(loc):
        tokenfiles = glob(os.path.join(loc, '*.yaml'))
        shuffle(tokenfiles)
    elif os.path.isfile(loc):
        tokenfiles = [loc]
    else:
        raise ValueError('No valid token location specified.')
    for f in tokenfiles:
        token = token_func(f)
        if verify_func(token):
            return token
            break
        else:
            continue
    raise NoValidTokenError('None of the tokens are working.')
