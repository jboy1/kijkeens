'''Usage:
  kijkeens twitter [-s] [-v | -vv] [--queue=<delay>] <config_file>
  kijkeens twitter worker [-v | -vv] <config_file>
  kijkeens (-h | --help)
  kijkeens --version

Arguments:
  <config_file>    Configuration file.

Options:
  -v -vv           Provide more/very detailed output.
  -s               Use Search API (Streaming API is default).
  --queue=<delay>  Enqueue results for n seconds.
  -h --help        Get this help.
  --version        Display version information.
'''
from __future__ import print_function
from docopt import docopt

from kijkeens.args import handle_args
from kijkeens.utils import get_version


def main():
    arguments = docopt(__doc__, version=get_version())
    Runner = handle_args(arguments)
    Runner.loop()
