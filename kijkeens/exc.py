# coding=utf-8
class NoValidTokenError(Exception):
    pass


class MissingConfigFileError(Exception):
    pass


class MissingConfigKeyError(Exception):
    pass


class NoValidEndpointError(Exception):
    pass
