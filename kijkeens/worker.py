# coding=utf-8
from time import sleep
from sqlalchemy.exc import IntegrityError, DataError

from .config import load_config_file, get_valid_token
from .queue import UniqueQueue
from .db import init_db
from .tw import make_tweets_table, tw_to_columns, tw_token, tw_verify_token, tw_show
from .exc import MissingConfigKeyError
from .utils import get_database_name


class Worker(object):
    def __init__(self, worker_type, config_file, logger):
        self.logger = logger
        self._supported_types = frozenset({'twitter'})
        self._pause = 0.5
        self.type = worker_type.lower()
        if self.type not in self._supported_types:
            raise ValueError('Invalid type for {}: {}.'.format(__name__, self.type))
        token_loc, _, database, table, queue_name = load_config_file(config_file)
        if not token_loc or not database or not table or not queue_name:
            raise MissingConfigKeyError
        if self.type == 'twitter':
            self.api_token = get_valid_token(token_loc, tw_token, tw_verify_token)
            self.db_insert = init_db(database, table, make_tweets_table)
        self.queue = UniqueQueue(queue_name)
        self.database = database
        self.table = table
        self.logger.info(u'Working on queue %s, writing results to %s/%s',
                         queue_name,
                         get_database_name(self.database), 
                         self.table)

    def loop(self):
        try:
            while True:
                job = self.queue.get()
                if job:
                    result = self._tw_task(job)
                    if result:
                        self.logger.info(u'%s: complete', job.decode('utf-8'))
                        self.queue.complete(job)
        except (KeyboardInterrupt, SystemExit):
            self.logger.info('Quitting.')
            pass

    def _tw_task(self, job):
        try:
            tweet = tw_show(job, self.api_token)
        except Exception as e:
            self.logger.debug(e)
            return False
        if not tweet:
            self.logger.warning(u'%s: invalid', job.decode('utf-8'))
            self.queue.complete(job)
            return False
        try:
            self.db_insert(**tw_to_columns(tweet))
        except IntegrityError:
            self.logger.warning('Tweet already in table %s', self.table)
        sleep(self._pause * 10)
        return True
