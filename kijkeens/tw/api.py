# coding=utf-8
import requests
from yaml import load as yaml_load
from requests_oauthlib import OAuth1
try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

SEARCH_URL = u'https://api.twitter.com/1.1/search/tweets.json'
STREAM_URL = u'https://stream.twitter.com/1.1/statuses/filter.json'
SHOW_URL = u'https://api.twitter.com/1.1/statuses/show.json'
VERIFY_URL = u'https://api.twitter.com/1.1/account/verify_credentials.json'


def tw_stream(query, token, params={}):
    params['track'] = query
    r = requests.post(STREAM_URL, data=params, auth=token, stream=True)
    return r


def tw_search(query, token, max_id=None, since='2006-03-21', count=100, params={}):
    params['q'] = u' since:'.join([query, since])
    params['count'] = count
    if max_id:
        params['max_id'] = max_id
    encoded_params = {k: str(v).encode('utf-8') for k, v in params.items()}
    r = requests.get('?'.join([SEARCH_URL, urlencode(encoded_params)]),
                     auth=token)
    if r.status_code < 400:
        j = r.json()
        if 'statuses' in j.keys() and len(j['statuses']):
            return j['statuses'], min([t['id'] for t in j['statuses']])


def tw_show(tweet_id, token, params={}):
    params['id'] = tweet_id
    r = requests.get('?'.join([SHOW_URL, urlencode(params)]),
                     auth=token)
    if r.status_code < 400:
        j = r.json()
    else:
        j = False
    return j


def tw_verify_token(token):
    params = {'skip_status': True, 'include_entities': False}
    r = requests.get('?'.join([VERIFY_URL, urlencode(params)]),
                     auth=token)
    return r.status_code == 200


def tw_token(file):
    with open(file) as tokenfile:
        token = yaml_load(tokenfile)
    return OAuth1(token['api_key'],
                  client_secret=token['api_secret'],
                  resource_owner_key=token['token'],
                  resource_owner_secret=token['token_secret'])
