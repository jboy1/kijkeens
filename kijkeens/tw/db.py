# coding=utf-8
from sqlalchemy import Table, Column, BigInteger, String, DateTime
from sqlalchemy.dialects.postgres import JSONB


def make_tweets_table(table_name, metadata):
    return Table(table_name, metadata,
                 Column('id', BigInteger, primary_key=True),
                 Column('text', String),
                 Column('lang', String),
                 Column('timestamp', DateTime),
                 Column('user', JSONB),
                 Column('geo', JSONB),
                 Column('place', JSONB),
                 Column('coordinates', JSONB),
                 Column('entities', JSONB),
                 Column('metadata', JSONB),
                 Column('misc', JSONB))


def tw_to_columns(tweet):
    try:
        tweet.pop('_id')
    except:
        pass
    r = {}
    r['id'] = tweet.pop('id')
    r['text'] = tweet.pop('text', None)
    r['lang'] = tweet.pop('lang', None)
    r['timestamp'] = tweet.pop('created_at', None)
    r['user'] = tweet.pop('user', None)
    r['geo'] = tweet.pop('geo', None)
    r['place'] = tweet.pop('place', None)
    r['coordinates'] = tweet.pop('coordinates', None)
    r['entities'] = tweet.pop('entities', None)
    r['metadata'] = tweet.pop('metadata', None)
    r['misc'] = tweet
    return r
