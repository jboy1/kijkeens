# coding=utf-8
import time
from redis import Redis


class UniqueQueue(object):
    '''Task queue with delayed jobs. Items can only be added once, and once
    they are marked complete, they cannot be readded.'''
    # heavily inspired by http://www.saltycrane.com/blog/2011/11/unique-python-redis-based-queue-delay/
    def __init__(self, name, host='localhost'):
        self._redis = Redis(host)
        self._record = '-'.join(['rhrhDrhdvaH', name])
        self.name = name

    def put(self, job, delay=0):
        if not self.has_item(job) and not self.recorded(job):
            offset = time.time() + delay
            result = self._redis.zadd(self.name, job, offset)
            return result > 0
        else:
            return False

    def get(self): 
        # TODO: Turn this into a reserve function so that two workers running at the same time
        # will try to complete the same job.
        min_t = 0
        max_t = time.time()
        item = self._redis.zrangebyscore(self.name,
                                         min_t,
                                         max_t,
                                         start=0,
                                         num=1,
                                         withscores=False)
        if item is None or not len(item):
            return False
        else:
            return item[0]

    def complete(self, job):
        removed = self.remove(job)
        if removed:
            result = self.add_to_record(job)
            return result > 0
        else:
            return False
    
    def remove(self, job):
        result = self._redis.zrem(self.name, job)
        return result > 0

    def has_item(self, job):
        return self._redis.zrank(self.name, job) is not None

    def recorded(self, job):
        return self._redis.sismember(self._record, job)

    def add_to_record(self, job):
        result = self._redis.sadd(self._record, job)
        return result > 0

    def __contains__(self, job):
        return self.has_item(job)

    def __repr__(self):
        return 'UniqueQueue({})'.format(self.name)
