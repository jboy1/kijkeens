# coding=utf-8
from functools import partial
from sqlalchemy import create_engine, MetaData


def init_db(db, table_name, table_func):
    engine = create_engine(db)
    metadata = MetaData()
    table = table_func(table_name, metadata)
    metadata.create_all(engine)
    conn = engine.connect()
    ins = table.insert()
    return partial(conn.execute, object=ins)
