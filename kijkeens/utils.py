# coding=utf-8
try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse


def get_version():
    version = (0, 3, 2)
    return '.'.join(map(str, version))


def get_database_name(dbpath):
    return urlparse(dbpath).path.strip('/')
