# coding=utf-8
import sys
import logging

from .kijker import Kijker
from .worker import Worker


def handle_args(args):
    logger = _set_logger(args['-v'])
    delay = _set_delay_time(args['--queue'])
    params = {'config_file': args['<config_file>'], 'logger': logger}
    if args['twitter'] and args['worker']:
        return Worker('twitter', **params)
    elif args['twitter'] and not args['-s']:
        return Kijker('twitter', delay=delay, **params)
    elif args['twitter'] and args['-s']:
        return Kijker('twitter_search', delay=delay, **params)


def _set_logger(verbosity=0):
    logger = logging.getLogger(__name__.split('.')[0])
    log_level = logging.WARNING
    if verbosity == 2:
        log_level = logging.DEBUG
    elif verbosity == 1:
        log_level = logging.INFO
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(logging.Formatter('[%(asctime)s] %(message)s',
                                           '%Y-%m-%d %H:%M:%S'))
    logger.addHandler(handler)
    return logger


def _set_delay_time(arg):
    def _num_part(s):
        f = ''.join(filter(lambda c: c.isdigit() or c == '.', s))
        return float(f) if f else None

    def _str_part(s):
        return ''.join(filter(lambda c: c.isalpha(), s))

    time_units = {'day': 60 * 60 * 24, 'days': 60 * 60 * 24,
                  'hour': 60 * 60, 'hours': 60 * 60,
                  'min': 60, 'mins': 60,
                  'sec': 1, 's': 1}
    delay = arg or 0
    try:
        delay = float(delay)
    except ValueError:
        num = _num_part(delay)
        unit = _str_part(delay).lower()
        if unit in time_units.keys() and num:
            delay = time_units[unit] * num
        else:
            delay = 0
    return delay
