from setuptools import setup, find_packages
from kijkeens.utils import get_version


requirements = ['redis==2.10.5',
                'requests==2.10.0',
                'requests-oauthlib==0.6.1',
                'docopt==0.6.2',
                'PyYAML==3.11',
                'SQLAlchemy==1.0.13',
                'psycopg2==2.6.1']

setup(name='kijkeens',
      version=get_version(),
      description='kijkeens is a research tool to gather data from social media',
      author='John D. Boy',
      url='https://bitbucket.org/jboy1/kijkeens',
      packages=find_packages(),
      install_requires=requirements,
      entry_points={'console_scripts': ['kijkeens = kijkeens.main:main']})
